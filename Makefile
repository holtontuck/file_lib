CFLAGS += -Wall -Werror -Wextra -Wpedantic -Wfloat-equal -Wwrite-strings -Wvla -Winline
CFLAGS += -D_DEFAULT_SOURCE -std=c11 -Waggregate-return
LD_FLAGS +=-L.
LD_LIBS += -lcunit

.PHONY:clean debug profile check

NAME=test

test_executable: LDLIBS+=-lfile_lib 
test_executable: CUnit_example.o libfile_lib.so
	$(CC) $(CFLAGS) $(LDFLAGS) CUnit_example.o -lcunit -L. -lfile_lib -o $(NAME)

test: test_executable
test: 
	export LD_LIBRARY_PATH=.; ./$(NAME)

libfile_lib.so: file_lib.o
	$(CC) file_lib.o $(CFLAGS) $(LDFLAGS) -shared -o libfile_lib.so

clean:
	$(RM) core *.o *.so* profile myTestFile.txt gmon.out $(NAME)

check:
	make -B 
	export LD_LIBRARY_PATH=.; valgrind --leak-check=full  -v ./$(NAME)

profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: test_executable
	export LD_LIBRARY_PATH=.; ./test
	
