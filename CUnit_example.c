#include <CUnit/Basic.h>
#include <CUnit/CUnit.h>
#include <CUnit/TestDB.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "file_lib.h"
#include "statx.h"


int init_suite1(void)
{
//This is where you put any initialization tasks that need to be done
return 0;
}

// The suite cleanup function.
int clean_suite1(void)
{
//This is where you put any cleanup tasks (free, close file, etc) 
//that need to be done
return 0;
}

/* Simple test of open_file()
 * Attempts to open a file for writing
 */
void open_close(void)
{
	FILE *fp = open_file("myTestFile.txt", "w+");
	CU_ASSERT_PTR_NOT_NULL(fp);
	if (fp == NULL)
	{
		fprintf(stderr, "Error opening file\n");
		exit(1);
	}

	close_file(fp);
}


void open_write_close(void)
{
	size_t bytes_written = 0;
	FILE *fp = open_file("myTestFile.txt", "w+");
	CU_ASSERT_PTR_NOT_NULL(fp);
	if (fp == NULL)
	{
		fprintf(stderr, "Error opening file\n");
		exit(1);
	}
	char test_string[] = "This is a test string. I am attempting to write this to a file. Will this work???";
	bytes_written = write_data(test_string, strlen(test_string), 1, fp);
	CU_ASSERT_EQUAL(bytes_written, 1);
	if (bytes_written !=1)
	{
		fprintf(stderr, "Error writing to file\n");
		fprintf(stderr, "Bytes written: %zu\n", bytes_written);
		close_file(fp);
	}
	close_file(fp);
	long long total = 0;
	for (int i = 0; i < 10000000; ++i)
	{
		total += i;
	}
}


void open_read_close(void)
{
	ssize_t bytes_read = 0;
	FILE *fp = open_file("myTestFile.txt", "r");
	CU_ASSERT_PTR_NOT_NULL(fp);
	if (fp == NULL)
	{
		fprintf(stderr, "Error opening file\n");
		exit(1);
	}
	char buffer[255];
	memset(buffer, 0, sizeof(buffer));
	if (buffer == NULL)
	{
		fprintf(stderr, "Couldn't allocate memory for buffer. Aborting.\n");
		close_file(fp);
		return;
	}
	bytes_read = read_data(buffer, 255, 1, fp);
	CU_ASSERT_EQUAL(bytes_read, strlen(buffer));
	if ((size_t)bytes_read != strlen(buffer))
	{
		fprintf(stderr, "Error reading from file\n");
		fprintf(stderr, "Bytes read: %zu\n", bytes_read);
		fprintf(stderr, "buffer length: %zu\n", strlen(buffer));
		close_file(fp);
		return;
	}
	close_file(fp);
	printf("GOT THIS: %s\n", buffer);
}


void open_file_non_exist(void)
{
	FILE *fp = open_file("not_a_file", "r");
	CU_ASSERT_PTR_NULL(fp);
	if (fp != NULL)
	{
		close_file(fp);
	}
}


void check_permissions(void)
{
	int ret_val = 0;
	ret_val = get_attrib("myTestFile.txt");
	CU_ASSERT_NOT_EQUAL(ret_val, 1);
}	

void set_check_permissions(void)
{
	int ret_val = 0;
	FILE *fp = open_file("file_for_permissions_testing.txt", "w+");
	CU_ASSERT_PTR_NOT_NULL(fp);
	if (fp == NULL)
	{
		fprintf(stderr, "Couldn't open file.\n");
		return;
	}
	close_file(fp);
	ret_val = get_attrib("file_for_permissions_testing.txt");
	CU_ASSERT_EQUAL(ret_val, 0);
	ret_val = set_attrib("file_for_permissions_testing.txt", "751");
	CU_ASSERT_NOT_EQUAL(ret_val, -1);
	ret_val = get_attrib("file_for_permissions_testing.txt");
	CU_ASSERT_EQUAL(ret_val, 0);
	ret_val = del_file("file_for_permissions_testing.txt");
	CU_ASSERT_EQUAL(ret_val, 0);
}
void get_file_creation_time(void)
{
	time_t ret_val = 0;
	ret_val = get_create_time("myTestFile.txt");
	CU_ASSERT_NOT_EQUAL(ret_val, 1);
	printf("\nCREATION TIME: %s\n", ctime(&ret_val));
}


void get_file_modify_time(void)
{
	time_t ret_val = 0;
	ret_val = get_modify_time("myTestFile.txt");
	CU_ASSERT_NOT_EQUAL(ret_val, -1);
	printf("\nCREATION TIME: %s\n", ctime(&ret_val));
}


void delete_file(void)
{
	int ret_val = 0;
	FILE *fp = open_file("delete_this", "w+");
	CU_ASSERT_PTR_NOT_NULL(fp);
	close_file(fp);
	//Shouldn't be NULL if the file actually exists
	ret_val = del_file("delete_this");
	CU_ASSERT_EQUAL(ret_val, 0);
	fp = open_file("delete_this", "r");
	//Should return NULL this time, as the file should no longer exist
	CU_ASSERT_PTR_NULL(fp);
}


/* The main() function for setting up and running the tests.
 * Returns a CUE_SUCCESS on successful running, another
 * CUnit error code on failure.
 */
int main()
{
   CU_pSuite pSuite = NULL;

   /* initialize the CUnit test registry */
   if (CUE_SUCCESS != CU_initialize_registry())
      return CU_get_error();

   /* add a suite to the registry */
   pSuite = CU_add_suite("Suite_1", NULL, NULL);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to the suite */
   if (CU_add_test(pSuite, "test of open_close", open_close) == NULL)
   {
      CU_cleanup_registry();
      return CU_get_error();
   }
   if (CU_add_test(pSuite, "test of open_write_close", open_write_close) == NULL)
   {
      CU_cleanup_registry();
      return CU_get_error();
   }
   if (CU_add_test(pSuite, "test of open_read_close", open_read_close) == NULL)
   {
      CU_cleanup_registry();
      return CU_get_error();
   }
   if (CU_add_test(pSuite, "get file modification time", get_file_modify_time) == NULL)
   {
      CU_cleanup_registry();
      return CU_get_error();
   }
   if (CU_add_test(pSuite, "attempt to open a nonexistent file for reading", open_file_non_exist) == NULL)
   {
      CU_cleanup_registry();
      return CU_get_error();
   }
   if (CU_add_test(pSuite, "Check File Permissions", check_permissions) == NULL)
   {
      CU_cleanup_registry();
      return CU_get_error();
   }
   if (CU_add_test(pSuite, "get file creation time", get_file_creation_time) == NULL)
   {
      CU_cleanup_registry();
      return CU_get_error();
   }
   if (CU_add_test(pSuite, "attempt to delete a file. Check to see if it can be opened after deleting it.", delete_file) == NULL)
   {
      CU_cleanup_registry();
      return CU_get_error();
   }
   if (CU_add_test(pSuite, "attempt to set file permissions.", set_check_permissions) == NULL)
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}


