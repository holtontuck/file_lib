#include <ctype.h>// use for toupper
#include <errno.h>
#include <fcntl.h> 
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <sys/types.h>

#include <unistd.h>

#include "file_lib.h"
#include "statx.h"

char *error_string;


int main(void)
{	
	size_t ret_val = 0;
	FILE * fp;
	ret_val	= set_attr("testFile.txt", "421");
	if (ret_val)
	{
		perror("Error");
	}
		
	//Test #1
	size_t buf_size = 2000;
	char buf[2000];
	size_t bytes_read = 0;
	errno = 0;
	fp = open_file("testFile.txt", "r");
	ret_val = get_attrib("/home/user/file_lib/test");
	if (ret_val != 0)
	{
		perror("Error in get_attrib");
		exit(errno);
	}

	if (fp == NULL)
	{
		if (strlen(error_string) > 0)
		{
			printf("%s\n", error_string);
			exit(errno);
		}
		else
		{
			perror("Error");
			printf("\n");
			exit(errno);

		}
	}

	bytes_read = read_data(buf, buf_size, 1, fp);
	if (bytes_read == 0 && strlen(buf) == 0)
	{
		if (strlen(error_string) > 0)
		{
			printf("%s\n", error_string);
			exit(errno);
		}
		else
		{
			perror("Error");
			printf("\n");
			exit(errno);
		}
	}
	printf("%s\n", buf);
	close_file(fp);

	//Test #2
	size_t bytes_written = 0;
	errno = 0;
	const char *write_buf = "HOW ABOUT this one???? Is this write test working?>?????\nI sure hope so!!!";
	fp = open_file("testWrite", "w");
	if (fp == NULL)
	{
		if (strlen(error_string) > 0)
		{
			printf("%s\n", error_string);
			exit(errno);
		}
		else
		{
			perror("Error");
			printf("\n");
			exit(errno);

		}
	}
	bytes_written = write_data((void *)write_buf, strlen(write_buf), 1, fp);
	if (bytes_written == 0 && strlen(write_buf) == 0)
	{
		if (strlen(error_string) > 0)
		{
			printf("%s\n", error_string);
			exit(errno);
		}
		else
		{
			perror("Error");
			printf("\n");
			exit(errno);
		}
	}
	close_file(fp);
	del_file("testWrite");

	time_t c_time = get_create_time("test");
	time_t m_time = get_modify_time("test");
	printf("%s", ctime(&c_time));
	printf("%s", ctime(&m_time));
}

