#ifndef FILE_LIB_H
#define FILE_LIB_H


#include <time.h>

enum ERRORS
{
	INVALID_MODE = -6,
	FILENAME_TOO_LONG,
	EMPTY_BUFFER,
	INVALID_VALUE,
	NULL_PTR, 
	SUCCESS = 0, 
};


/**
 * This is a wrapper for the C standard fopen function.
 * @param name of file to open
 * @param mode (read, write, append) for file
 * @return file pointer
 */
FILE * 
open_file(const char* name, const char* mode);


/**
 * This is a wrapper for the C standard fclose function.
 * @param fp is a FILE pointer to the file you wish to close
 */
void
close_file(FILE *fp);


/**
 * This is a wrapper for the C standard fread function.
 * @param source :a pointer to an empty buffer to hold the data as it is read
 * @param size of the item in bytes
 * @param items of 'size' bytes to be read in
 * @param file: file pointer to the file where the data will be read from 
 * @return integer corresponding to the number of bytes read
 */
int
read_data(char *source, size_t size, size_t items, FILE *file);


/**
 * This is a wrapper for the C standard fwrite function.
 * @param source: a pointer to a buffer containing the data to be written
 * @param size of the item in bytes
 * @param items of 'size' bytes to be written out (how many items are there?)
 * @param file pointer to the file where the data will be read from 
 * @return integer corresponding to the number of bytes written
 */
int
write_data(char *source, size_t size, size_t items, FILE *file);


/**
 * This is a wrapper for the C function 'remove', which removes a file or directory.
 * @param path is the filename, directory name or filepath to be removed
 * @return integer, 0 for succes or -1 for failure, and set errno accordingly
 */
int
del_file(const char *path);


/**
 * This is a wrapper for 'statx' system call, which returns file stats.
 * @param path: the filename, directory name or filepath for which stats are desired
 * @return integer, 0 for succes or other code (see enum ERRORS above) for failure, and set errno accordingly
 */
int
get_attrib(const char *path);


/**
 * This is a wrapper for chmod.
 * @param path :the filename, directory name or filepath for which stats are desired
 * @param new_mode :three digit octal string ("755", for example) representing new permissions for user, group and owner
 * @return 0 for succes or other int for error (see enum ERRORS above), and set errno accordingly
 */
int
set_attrib(const char *path, const char* new_mode);


/**
 * This is a wrapper for 'statx' system call, which returns file stats.
 * @param path: the filename, directory name or filepath for which create_time (birth time) is desired
 * @return time_t value for success or other code (see enum ERRORS above) for failure, and set errno accordingly
 */
time_t
get_create_time(const char *path);


/**
 * This is a wrapper for 'statx' system call, which returns file stats.
 * @param path: the filename, directory name or filepath for which modify_time is desired
 * @return time_t value for succes or other code (see enum ERRORS above) for failure, and set errno accordingly
 */
time_t
get_modify_time(const char *path);

#endif
