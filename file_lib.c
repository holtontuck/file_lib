#include <ctype.h>// use for toupper
#include <errno.h>
#include <fcntl.h> 
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <unistd.h>
#include "statx.h"
#include "file_lib.h"

#define __statx		332 // This is the number of the system call for statx


FILE * open_file(const char* name, const char* mode)
{
	// Validity checks on filename and mode
	
	size_t name_string_length = strnlen(name, 255);
	size_t mode_string_length = strnlen(mode, 3);
	if (name_string_length == 0)
	{
		errno = EINVAL; 
		return NULL;
	}

	if (name_string_length > 255)
	{
		errno = ENAMETOOLONG;
		return NULL;
	}

	if (mode_string_length == 0)
	{
		errno =  EINVAL;
		return NULL;
	}

	if (mode_string_length > 255)
	{
		errno = E2BIG;
		return NULL;
	}

	//Parse the mode string to determine the desired mode
	for (size_t i = 0; i < mode_string_length; ++i)
	{
		switch(mode[i])
		{
			case 'R':
			case 'r':
			case 'W':
			case 'w':
			case 'A':
			case 'a':
			case '+':
				break;
			// If any of the above is in the mode string--valid.
			default:
			// If not, the mode string is invalid
				errno = EINVAL;
				return NULL;
				break; // Doing this for consistency.
		}
	}
	FILE *fp = fopen(name, mode);
	//After checks have been completed, attempt to open the file
	return fp;
}


void close_file(FILE *fp)
{
	if (fp == NULL)
	{
		errno = EINVAL;
	}	

	fclose(fp);
}


int read_data(char *source, size_t size, size_t items, FILE *file)
{
	//Perform validity checks 
	if (source == NULL)
	{
		return NULL_PTR;
	}
	if (size < 1)
	{
		errno = EINVAL;
		return INVALID_VALUE;
	}
	if (items < 1)
	{
		errno = EINVAL;
		return INVALID_VALUE;
	}
	if (file == NULL)
	{
		errno = EBADFD;
		return NULL_PTR;
	}
	//Attempt to read from the file after checks are complete
	size_t bytes = fread(source, size, items, file);
	size_t source_string_length = strnlen(source, 255);
    if (bytes == 0 && source_string_length == 0)
    {
    
        perror("Error");
        printf("\n");
        return EMPTY_BUFFER;
    }
	return source_string_length;
	//Return the number of bytes read into the buffer
}


int write_data(char *source, size_t size, size_t items, FILE *file)
{
	//initialize the error_string to empty each time this function is called
	//Don't want any previous values, since this is a global variable
	size_t bytes = 0;

	//Perform validity checks
	if (source == NULL)
	{
		errno = EINVAL;
		return NULL_PTR;
	}

	if (size < 1)
	{
		errno = EINVAL;
		return INVALID_VALUE;
	}

	if (items < 1)
	{
		errno = EINVAL;
		return INVALID_VALUE;
	}

	if (file == NULL)
	{
		errno = EBADFD;
		return NULL_PTR;
	}
	
	bytes = fwrite(source, size, items, file);
	//Attempt to write to the file
	
	if (bytes == 0 && strnlen(source, 255) == 0)
	{
		//If nothing was in the write buffer
		{
			perror("Error");
			printf("\n");
			return EMPTY_BUFFER;
		}
	}
	return bytes;
}


int del_file(const char *path)
{
	//Perform validity checks
	size_t path_string_length = strnlen(path, 255);
	if (path == NULL ||  path_string_length == 0)
	{
                errno = EINVAL;
                return NULL_PTR;
	}

	if (path_string_length > 255)
	{
		errno = ENAMETOOLONG;
		return FILENAME_TOO_LONG;
	}

	size_t retval = remove(path);
	//Attempt to delete the file or directory
	return retval;
	//success = 0, failure = -1 and errno is set
}


int get_attrib(const char *path)
{
	struct statx fileStat = {0};
	int file = 0;
	int dirfd = AT_FDCWD;
	int flags = AT_SYMLINK_NOFOLLOW;
	unsigned int mask = STATX_ALL;

	//Perform validity checks
	if((file = open(path ,O_RDONLY)) == -1)
	{
	    	return -1;
		//failure = -1 and errno is set
	}

	if (syscall(__statx, dirfd, path, flags, mask, &fileStat) == -1)
	{
		return -1;
		//failure = -1 and errno is set
	}

	printf("Information for %s\n", path);
	printf("File Permissions: \t");
	printf( (S_ISDIR(fileStat.stx_mode)) ? "d" : "-");
	printf( (fileStat.stx_mode & S_IRUSR) ? "r" : "-");
	printf( (fileStat.stx_mode & S_IWUSR) ? "w" : "-");
	printf( (fileStat.stx_mode & S_IXUSR) ? "x" : "-");
	printf( (fileStat.stx_mode & S_IRGRP) ? "r" : "-");
	printf( (fileStat.stx_mode & S_IWGRP) ? "w" : "-");
	printf( (fileStat.stx_mode & S_IXGRP) ? "x" : "-");
	printf( (fileStat.stx_mode & S_IROTH) ? "r" : "-");
	printf( (fileStat.stx_mode & S_IWOTH) ? "w" : "-");
	printf( (fileStat.stx_mode & S_IXOTH) ? "x" : "-");
	printf("\n\n");	
	return 0;
}


int set_attrib(const char *path, const char* new_mode)
{
	//path is the filename or complete path of the file
	//whose permissions are to be altered
	
	// new_mode is a string comprised of one, two or three integer values
	// between zero and seven, signifying the new mode for UGO 
	// (user, group, other)
	char *ptr = NULL;
	//*ptr is used in conjunction with strtoul call below
	long mode = 0;

	//Perform validity checks
	if (path == NULL)
	{
		errno = EINVAL;
		return NULL_PTR;
	}
	if (new_mode == NULL)
	{
		errno = EINVAL;
		return NULL_PTR;
	}
	if (strnlen(new_mode, 3) != 3)
	//The mode should always be 3 digits long
	{
		errno = EINVAL;
		return INVALID_MODE;
	}

	mode = strtoul(new_mode, &ptr, 10);
	//convert the mode from a string to a number
	
	int num[3];
	//array to hold each of the three digits
	
	num[0] = mode / 100;
	
	//Do the math to separate the digits
	mode %= 100;
	num[1] = mode / 10;
	mode %= 10;
	num[2] = mode;

	for (size_t i = 0; i < 3; ++i)
	//check for invalid digits
	{
		if (num[i] <0 || num[i] > 7) 
			{
				errno = EINVAL;
				return INVALID_MODE;
			}
	}

	size_t multiplier;
	//multiplier is used to work through the powers of two for the binary math
	
	bool perm[3][3];
	//Convert from base ten(10) to base two(2)
		for (size_t i = 0; i < 3; ++i)
		{
			size_t multiplier = 4;
			for (size_t j = 0; j < 3; ++j)
			{
				perm[i][j] = num[i] & multiplier;
				multiplier /= 2;
			}
		}
	size_t total = 0;
	multiplier = 256;
	//start at 256 for multiplier, and work backwards to 1
	size_t temp = 0;
	for (size_t permission = 0; permission < 3; ++permission)
	{
		for (size_t digit = 0; digit < 3; ++digit)
		{
			if (perm[permission][digit])
			{
				temp += multiplier;
				total += temp;
			}
			temp = 0;
			multiplier /= 2;
		}
	}
	// "or" each bit with the multiplier
	// multiplier decreases by power of two, each iteration
	
	size_t retval = chmod(path, total);
	//Attempt to change the permissions, as requested
	return retval;
	//failure = -1 and errno is set
}


time_t get_create_time(const char *path)
{
	struct statx fileStat;
	int file = 0;
	int dirfd = AT_FDCWD;
	int flags = AT_SYMLINK_NOFOLLOW;
	unsigned int mask = STATX_ALL;

	//Perform validity checks
	if((file = open(path ,O_RDONLY)) == -1)
	{
		return -1;
		//failure = -1 and errno is set
	}

	if (syscall(__statx, dirfd, path, flags, mask, &fileStat) == -1)
	{
		return -1;
		//failure = -1 and errno is set
	}
	time_t c_time = (time_t)(fileStat.stx_btime.tv_sec);
	//Get the time stats
	return c_time;
}


time_t get_modify_time(const char *path)
{
	struct statx fileStat;
	int file = 0;
	int dirfd = AT_FDCWD;
	int flags = AT_SYMLINK_NOFOLLOW;
	unsigned int mask = STATX_ALL;

	//Perform validity checks
	if((file = open(path ,O_RDONLY)) == -1)
	{
		return -1;
		//failure = -1 and errno is set
	}

	if (syscall(__statx, dirfd, path, flags, mask, &fileStat) < 0)
	{
		return -1;
		//failure = -1 and errno is set
	}
	time_t m_time = (time_t)(fileStat.stx_mtime.tv_sec);
	//Get the time stats
	return m_time;
}





